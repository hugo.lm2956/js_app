var db = require('./sqlite_connection');

    var ActivityEntryDAO = function() {

        this.insert = function (values, callback) {

            db.run(`INSERT INTO Data
                    VALUES ("id_data", "time", "cardioFrequency", "latitude", "longitude", "altitude",
                            "theActivity") VALUES (?,?,?,?,?,?,?)`, values[0], values[1], values[2], values[3], values[4], values[5], values[6], function (err) {
                if (err) {
                    return console.log(err.message);
                }
                console.log(`A row has been inserted`);
                callback();
            });

        };

        this.delete = function(key, callback) {

            db.run(`DELETE
                    FROM Data
                    WHERE id_data = ?`, key, function (err) {
                if (err) {
                    return console.log(err.message);
                }
                callback();
            });
        }

        this.update = function(key, values, callback){

            this.delete(key,callback);
            this.insert(values, callback);

            callback();

        };

        this.findAll = function (callback){

            db.all("SELECT * FROM Data", [], (err, rows) => {
                if (err) {
                    throw err;
                }
                callback(rows);
            })
        };

        this.findByKey = function(key, callback){

            db.get("SELECT * FROM Data where id_data=?", key, (err, row) => {
                if (err) {
                    return console.error(err.message);
                }
                callback(row);

            });

        };

        this.deleteAll = function(callback){

            db.run(`DELETE * FROM Data`, (error) => {
                if (error) {
                    console.error(error);
                }

                callback();
            });

        };

    };

var dao = new ActivityEntryDAO();
module.exports = dao;